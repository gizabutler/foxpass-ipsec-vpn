#!/usr/bin/python
import subprocess
from os import chmod, chown, geteuid, urandom
from urllib2 import Request, urlopen
import ifaddr
from jinja2 import Environment, FileSystemLoader
from ec2_metadata import ec2_metadata

METADATA_BASE_URL = "http://169.254.169.254/"
private_ip = ['172.16.1.100', '172.16.0.100']

def get_address(private_ip):
    adapters = ec2_metadata.network_interfaces
    for adapter in adapters:
        ip = urlopen(METADATA_BASE_URL + 'latest/meta-data/network/interfaces/macs/' +
                     adapter + '/local-ipv4s/').read()
        if ip in private_ip:
            return ip
        
address = get_address(private_ip)


def get_gateway(address):
    if address == '172.16.0.100':
        gateway = '172.16.0.1'
        return gateway
    elif address == '172.16.1.100':
        gateway = '172.16.1.1'
        return gateway


gateway = get_gateway(address)

def write_to_cfg(address):
    # * write IP `to eth1.cfg
    file_loader = FileSystemLoader("/opt/templates/")
    env = Environment(loader=file_loader)
    template = env.get_template('eth1.cfg')
    output = template.render(PRIVATE_IP=address, GATEWAY=gateway)
    fname = "/etc/network/interfaces.d/eth1.cfg"
    with open(fname, 'w') as f:
        f.write(output)
    return output
    # * set /etc/network/interfaces.d/eth1.cfg to be owned and only accessible by root
    chmod('/etc/network/interfaces.d/eth1.cfg', 0600)
    chown('/etc/network/interfaces.d/eth1.cfg', 0, 0)

output = write_to_cfg(address)

def aws_config():
    region = ec2_metadata.region
    file_loader = FileSystemLoader("/opt/templates/")
    env = Environment(loader=file_loader)
    template = env.get_template('awsconfig')
    output = template.render(DEFAULT_REGION=region)
    fname = "/root/.aws/config"
    with open(fname, 'w') as f:
        f.write(output)
    return output


def restart_services():
    subprocess.call(['systemctl', 'restart', 'networking'], shell=False)
    subprocess.call(['systemctl', 'restart', 'pacemaker'], shell=False)
    subprocess.call(['systemctl', 'restart', 'corosync'], shell=False)


def main():
    get_address
    get_gateway
    write_to_cfg
    aws_config()
    restart_services()

if __name__ == '__main__':
    main()
